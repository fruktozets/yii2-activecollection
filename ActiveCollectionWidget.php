<?php

namespace fruktozets\activecollection;

use fruktozets\activecollection\assets\ActiveCollectionAsset;
use fruktozets\activecollection\assets\DropZoneAsset;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * Class ActiveCollectionWidget
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package common\widgets
 */
class ActiveCollectionWidget extends Widget
{
    const TEMPLATE_TEXT = 'text';
    const TEMPLATE_TEXTAREA = 'textarea';
    const TEMPLATE_NUMBER = 'number';
    const TEMPLATE_DROPZONE = 'dropzone';
    const TEMPLATE_CUSTOM = 'custom';

    /**
     * @var string
     */
    public $formId;
    /**
     * @var Model The model that this widget is associated with.
     */
    public $model;

    /**
     * @var ActiveRecord[].
     */
    public $data = [];

    /**
     *  [[
     *      'title',
     *      'desctiption' => ActiveCollectionWidget::TEMPLATE_TEXTAREA,
     *      'name' => [
     *          'template' => ActiveCollectionWidget::TEMPLATE_TEXT,
     *          'options' => [
     *              'class' => 'text-success',
     *           ]
     *      ],
     *      'image_id' => [
     *          'template' => ActiveCollectionWidget::TEMPLATE_DROPZONE,
     *          'widgetOptions' => [];
     *      ],
     *      [
     *          'attribute' => 'avatar_id',
     *          'template' => ActiveCollectionWidget::TEMPLATE_CUSTOM,
     *      ],
     *  ]]
     * @var array
     */
    public $fields = [];
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (!$this->model instanceof Model || !$this->formId) {
            throw new InvalidConfigException("Either 'model' and 'formId' properties must be specified.");
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        $this->rotateFields();
    }

    /**
     * @return void
     */
    protected function rotateFields()
    {
        $result = [];

        foreach ($this->fields as $index => $value) {
            $field = [];
            if (is_string($index) && is_string($value)) {
                $field = $this->formatField($index, $value);
            } elseif (is_int($index) && is_string($value)) {
                $field = $this->formatField($value);
            } elseif (is_array($value)) {
                $attribute = is_string($index) ? $index : $value['attribute'];
                $htmlOptions = $value['options'] ?? [];
                $field = $this->formatField($attribute, $value['template'], $htmlOptions);
                $this->widgetOptions($field, $value);
            }

            if ($validators = $this->getValidators($field['attribute'])) {
                $field['validate'] = $validators;
            }

            $result[] = $field;
        }

        $this->fields = $result;
    }

    /**
     * @param array $field
     * @param array $options
     *
     * @return void
     */
    protected function widgetOptions(array &$field, array $options)
    {
        if ($field['template'] == self::TEMPLATE_DROPZONE) {
            DropZoneAsset::register($this->view);
            $widgetOptions = Json::encode($options['widgetOptions'] ?? []);
            $field['callback'] = new \yii\web\JsExpression("activeCollection({$widgetOptions})");
        } elseif (isset($value['callback'])) {
            $field['callback'] = $options['callback'];
        }
    }

    /**
     * @param string $template
     * @param string $attribute
     * @param array $options
     * @return array
     */
    protected function formatField(string $attribute, string $template = null, array $options = []): array
    {
        return [
            'attribute' => $attribute,
            'options' => $options,
            'template' => $template,
            'label' => $this->model->getAttributeLabel($attribute),
        ];
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    protected function getJsOptions(): array
    {
        return [
            'formId' => $this->formId,
            'model' => $this->model->formName(),
            'id' => $this->getId(),
            'fields' => $this->fields,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        $this->registerClientScript();

        return Html::tag('div', '', [
            'id' => $this->getId(),
        ]);
    }

    /**
     * @param string $attribute
     * @return null|JsExpression
     */
    protected function getValidators(string $attribute)
    {
        $id = $this->options['id'];

        foreach ($this->model->getActiveValidators($attribute) as $validator) {
            /* @var $validator \yii\validators\Validator */
            $jsValidator = $validator->clientValidateAttribute($this->model, $attribute, $this->getView());

            if ($validator->enableClientValidation && $jsValidator != '') {
                if ($validator->whenClient !== null) {
                    $jsValidator = "if (({$validator->whenClient})(attribute, value)) { $jsValidator }";
                }
                $validators[] = $jsValidator;
            }
        }

        if (empty($validators)) {
            return null;
        }

        return new JsExpression('function (attribute, value, messages, deferred, $form) {' . implode('', $validators) . '}');
    }

    /**
     * @return array
     */
    protected function getData(): array
    {
        $result = [];

        $attributes = array_column($this->fields, 'attribute');

        foreach ($this->data as $model) {
            $field = [
                'prefix' => $model->getPrimaryKey(),
            ];

            foreach ($attributes as $attribute) {
                $field[$attribute] = $model->$attribute;
            }

            $result[] = $field;
        }

        return $result;
    }

    /**
     * Registers dropZone js plugin
     */
    protected function registerClientScript()
    {
        $js = [];
        $this->view->registerCss(".collection {opacity: 1!important;}");

        ActiveCollectionAsset::register($this->view);

        $options = Json::encode($this->getJsOptions());
        $data = Json::encode($this->getData());

        $js[] = "var activeCollectionWidget = new ActiveCollectionWidget({$options});";
        $js[] = "activeCollectionWidget.run({$data});";

        $this->view->registerJs(implode("\n", $js));
    }
}