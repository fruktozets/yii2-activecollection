<?php

namespace fruktozets\activecollection\assets;

use yii\web\AssetBundle;

/**
 * Class ActiveCollectionAsset
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\activecollection
 */
class ActiveCollectionAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/../source';

    public $js = [
        'active.collection.js',
    ];

    public $depends = [
        'yii\widgets\ActiveFormAsset',
    ];
}