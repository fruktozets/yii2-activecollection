<?php

namespace fruktozets\activecollection\assets;

use yii\web\AssetBundle;

/**
 * Class DropZoneAsset
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\activecollection
 */
class DropZoneAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/../source';

    public $js = [
        'dropzone.js',
    ];

    public $depends = [
        'fruktozets\dropzone\DropZoneAsset',
    ];
}