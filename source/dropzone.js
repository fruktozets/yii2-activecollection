var activeCollection = function (data) {
    var _ = this;
    var files = data.files || [];
    this.dropzones = [];

    var DropzoneConfig = function (widget, field) {
        this.widget = widget;
        this.field = field;
        this.instance = null;
    };

    DropzoneConfig.prototype.run = function () {
        var id = this.field.getId() + '-dropzone';
        var dropzone = document.createElement('div');
        dropzone.id = id;
        dropzone.className = 'dropzone';
        this.field.container.appendChild(dropzone);
        this.instance = new Dropzone('#' + id, data);
        _.dropzones.push(this.instance);

        this.files();
        this.decrementMaxFiles();
        this.instance.on('success', this.success(this));
        this.instance.on('removedfile', this.removedfile(this));
    };

    DropzoneConfig.prototype.decrementMaxFiles = function () {
        var prefix = this.field.parent.prefix;

        if (files.hasOwnProperty(prefix)) {
            if (this.instance.options.maxFiles > 0) {
                this.instance.options.maxFiles = this.instance.options.maxFiles - files[prefix].length;
            }
        }
    };

    DropzoneConfig.prototype.success = function (obj) {
        return function (file, response) {
            var fileIds = [];
            file.id = response.id;

            for (var i = 0; i < obj.instance.files.length; i++) {
                fileIds.push(obj.instance.files[i].id);
            }

            obj.field.input.value = JSON.stringify(fileIds);
        };
    };

    DropzoneConfig.prototype.removedfile = function (obj) {
        return function (file) {
            var fileIds = [];

            for (var i = 0; i < obj.instance.files.length; i++) {
                fileIds.push(obj.instance.files[i].id);
            }

            obj.field.input.value = JSON.stringify(fileIds);
        };
    };

    DropzoneConfig.prototype.files = function () {
        var prefix = this.field.parent.prefix;
        if (files.hasOwnProperty(prefix)) {
            var itemFiles = files[prefix];

            for (var i = 0; i < itemFiles.length; i++) {
                var mockFile = itemFiles[i];
                this.instance.emit("addedfile", mockFile);
                this.instance.files.push(mockFile);
                this.instance.emit("thumbnail", mockFile, mockFile.thumbnail);
                this.instance.emit("complete", mockFile);
            }
        }

        var fileIds = [];

        for (var i = 0; i < this.instance.files.length; i++) {
            fileIds.push(this.instance.files[i].id);
        }

        this.field.input.value = JSON.stringify(fileIds);
    };

    return function (field) {
        var dropzone = new DropzoneConfig(this, field);
        dropzone.run();
    };
};