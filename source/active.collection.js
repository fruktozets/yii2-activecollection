(function () {
    this.ActiveCollectionWidget = function () {
        this.addButton = null;
        this.container = null;
        this.prefix = 0;
        this.collections = [];

        this.options = {
            addButton: true,
            removeButton: true,
        };

        if (arguments[0] && typeof arguments[0] === "object") {
            this.options = mergeOptions(this.options, arguments[0]);
        }
    };

    ActiveCollectionWidget.prototype.run = function (data) {
        this.build();
        var _ = this;

        data.forEach(function (datum) {
            _.prefix = datum.prefix;
            _.create(datum);
        });
    };

    ActiveCollectionWidget.prototype.build = function () {
        var wrapper = document.getElementById(this.options.id);
        this.container = document.createElement('div');
        wrapper.appendChild(this.container);
        this.container.className = 'row';

        if (this.options.addButton === true) {
            var plus = document.createElement('span');
            plus.className = "glyphicon glyphicon-plus";

            this.addButton = document.createElement("a");
            this.addButton.className = "add-button";
            this.addButton.href = "#";
            this.addButton.appendChild(plus);
            wrapper.appendChild(this.addButton);
            this.addButton.addEventListener('click', this.addButtonListener.bind(this));
        }
    };

    ActiveCollectionWidget.prototype.getPrefix = function () {
        return this.prefix++;
    };

    ActiveCollectionWidget.prototype.addButtonListener = function (e) {
        e.preventDefault();

        this.create(null);
    };

    ActiveCollectionWidget.prototype.create = function (values) {
        var _ = this;
        var collection = new Collection({
            id: _.options.id,
            model: _.options.model,
            prefix: _.getPrefix()
        });

        this.add(collection);

        this.options.fields.forEach(function (data) {
            var field = new Field({
                attribute: data.attribute,
                label: data.label,
                template: data.template,
                value: values ? values[data.attribute] : null,
                validate: data.validate
            })

            collection.add(field);

            if (typeof data.callback === 'function') {
                data.callback.call(_, field);
            }
        });

        collection.initializeYiiActiveFields();

        setTimeout(function () {
            collection.show();
        });
    };

    // ActiveCollectionWidget.prototype.rotate = function () {
    //
    //     for (var i = 0; i < this.collections.length; i++) {
    //         this.collections[i].prefix = i;
    //         this.collections[i].rebuild();
    //     }
    // };

    ActiveCollectionWidget.prototype.add = function (collection) {
        if (!collection instanceof Collection) {
            return false;
        }

        var _ = this;
        //this.addButton.className += " disable";

        collection.parent = this;
        this.collections.push(collection);
        collection.build();

        collection.container.addEventListener(_.transitionEnd, function () {
            _.addButton.className = _.addButton.className.replace(" disable", "");
        });
    };

    var Collection = function () {
        this.id = null;
        this.model = null;
        this.prefix = null;
        this.visibleClass = 'collection';
        this.parent = null;
        this.removeButton = null;
        this.transitionEnd = transitionSelect();
        this.container = null;

        this.fields = [];

        if (arguments[0] && typeof arguments[0] === "object") {
            mergeProperties.call(this, arguments[0]);
        }
    };

    Collection.prototype.build = function () {
        var wrapper = document.createElement("div");
        this.container = document.createElement("div");
        wrapper.appendChild(this.container);
        wrapper.className = 'col-lg-3';

        if (this.parent.options.removeButton) {
            this.removeButton = document.createElement("a");
            this.removeButton.className = "remove";
            this.removeButton.href = "#";
            this.removeButton.innerText = "remove";
            wrapper.appendChild(this.removeButton);

            this.removeButton.addEventListener('click', this.remove.bind(this));
        }

        this.parent.container.appendChild(wrapper);
        this.container.style = 'opacity:0; transition: opacity .3s linear';
    };

    Collection.prototype.show = function () {
        this.container.className = this.visibleClass;
    };

    Collection.prototype.hide = function () {
        this.container.className = this.container.className.replace(this.visibleClass, "");
    };

    Collection.prototype.rebuild = function () {
        var widget = this.parent;

        this.fields.forEach(function (field) {
            jQuery('#' + widget.options.formId).yiiActiveForm('remove', field.getId());
            field.setSelectors();
        });

        this.initializeYiiActiveFields();
    };

    Collection.prototype.remove = function (e) {
        e.preventDefault();
        var _ = this;
        var widget = this.parent;

        for (var i = 0; i < this.parent.collections.length; i++) {
            if (this.parent.collections[i] == this) {
                this.parent.collections.splice(i, 1);

                this.container.addEventListener(_.transitionEnd, function () {
                    _.container.parentNode.remove();
                });

                this.hide();
            }
        }

        this.fields.forEach(function (field) {
            console.log('#' + widget.options.formId);
            jQuery('#' + widget.options.formId).yiiActiveForm('remove', field.getId());
        });

        //this.parent.rotate();

        console.log(this.parent);
    };

    Collection.prototype.add = function (field) {
        if (!field instanceof Field) {
            return false;
        }

        field.parent = this;
        this.fields.push(field);
        field.build();
        this.container.appendChild(field.container);
    };

    Collection.prototype.initializeYiiActiveFields = function () {
        var widget = this.parent;
        var $form = jQuery('#' + widget.options.formId);

        this.fields.forEach(function (field) {
            if ($form.data('yiiActiveForm')) {
                $form.yiiActiveForm('add', field.getAttribute());
            } else {
                $form.on('afterInit', function () {
                    jQuery(this).yiiActiveForm('add', field.getAttribute());
                });
            }
        })
    };

    var Field = function () {
        this.container = null;
        this.input = null;
        this.label = null;
        this.value = null;
        this.parent = null;
        this.attribute = null;
        this.template = null;
        this.validate = null;

        if (arguments[0] && typeof arguments[0] === "object") {
            mergeProperties.call(this, arguments[0]);
        }
    };

    Field.prototype.getId = function () {
        return this.parent.id + '-' + this.parent.prefix + '-' + this.parent.model + '-' + this.attribute;
    };

    Field.prototype.getContainerClass = function () {
        return this.getId() + '-field';
    };

    Field.prototype.getName = function () {
        return this.parent.model + '[' + this.parent.prefix + '][' + this.attribute + ']';
    };

    Field.prototype.getAttribute = function () {
        return {
            container: "." + this.getContainerClass(),
            id: this.getId(),
            input: "#" + this.getId(),
            name: this.getName(),
            validate: this.validate
        };
    };

    Field.prototype.setSelectors = function () {
        this.container.className = 'form-group ' + this.getContainerClass();
        this.input.id = this.getId();
        this.input.name = this.getName();
    };

    Field.prototype.createFieldElement = function () {
        if (this.template === 'textarea') {
            this.input = document.createElement("textarea");
            this.input.innerText = this.value;

            return;
        }

        this.input = document.createElement("input");
        this.input.value = this.value;

        switch (this.template) {
            case 'text':
            case 'number':
                this.input.type = this.template;
                break;
            default:
                this.input.type = 'hidden';
        }
    };

    Field.prototype.build = function () {
        this.container = document.createElement("div");
        var controlLabel = document.createElement('label');

        controlLabel.className = 'control-label';
        controlLabel.innerText = this.label;
        controlLabel.for = this.getId();
        this.container.appendChild(controlLabel);
        this.createFieldElement();
        this.setSelectors();
        this.input.className = 'form-control';
        this.container.appendChild(this.input);

        var helpBlock = document.createElement('div');
        helpBlock.className = 'help-block';
        this.container.appendChild(helpBlock);
    };

    function mergeProperties(properties) {
        for (property in properties) {
            if (this.hasOwnProperty(property)) {
                this[property] = properties[property];
            }
        }
    }

    function transitionSelect() {
        var el = document.createElement("div");
        if (el.style.WebkitTransition) return "webkitTransitionEnd";
        if (el.style.OTransition) return "oTransitionEnd";
        return 'transitionend';
    }

    function mergeOptions(source, properties) {
        var property;

        for (property in properties) {
            source[property] = properties[property];
        }

        return source;
    }
}());