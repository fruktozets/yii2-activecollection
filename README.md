# yii2-activecollection
This is the unsuccessful project for creating and validate collections for a linked model.
It's working correctly but the code is very difficult because of Yii framework architecture is not support a feature like this. 
### Example:
```php
ActiveCollectionWidget::widget([
    'model' => $customer, // Model for creating a template.
    'data' => $customerModels, // Existing models for initialization. 
    'formId' => $form->id,
    'fields' => [
        [
            'attribute' => 'image_id',
            'template' => ActiveCollectionWidget::TEMPLATE_DROPZONE,
            'widgetOptions' => [
                "addRemoveLinks" => "true",
                "acceptedFiles" => "image/*",
                "dictDefaultMessage" => "Drop file here or click to upload",
                "url" => "/image/upload",
                "maxFiles" => 1,
        ],
        'fio' => ActiveCollectionWidget::TEMPLATE_TEXT,
        'zipcode' => ActiveCollectionWidget::TEMPLATE_NUMBER,
        'position' => ActiveCollectionWidget::TEMPLATE_TEXTAREA,
    ],
]) 
``` 

   